#!/usr/bin/python3

from getpass import getpass

from web import db, models

db.create_all()


def add_user():
    username=input('username: ')
    password=getpass('password: ')

    u=models.Users()
    u.username=username
    u.set_password(password)

    db.session.add(u)
    db.session.commit()


if __name__ == '__main__':
    add_user()
