from flask import request, redirect, url_for, flash, render_template, Blueprint
from flask_login import current_user, login_user, logout_user, login_required

from .forms import LoginForm
from ..models import Users

auth=Blueprint('auth', __name__, url_prefix='/auth')


@auth.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form=LoginForm()
    if form.validate_on_submit() or request.method == 'POST':
        u=(Users.query.filter_by(username=form.username.data)).first()
        if u is None or not u.check_password(form.password.data):
            flash('error Invalid username or password')
            return render_template('auth/login.html', form=form)
        login_user(u)
        return redirect(url_for('index'))
    return render_template('auth/login.html', form=form)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('auth.login'))
