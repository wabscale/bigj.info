from flask import Flask, render_template, redirect, url_for
from flask_bootstrap import Bootstrap
from flask_login import login_required
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import CSRFProtect

from .config import Config

host='0.0.0.0'
port=5000

app=Flask(__name__, static_url_path='/static')
app.config.from_object(Config())

Bootstrap(app)
CSRFProtect(app)

mail=Mail(app)
db=SQLAlchemy(app)

# register blueprints
from .auth import auth
from .files import files
from .api import api

app.register_blueprint(auth)
app.register_blueprint(files)
app.register_blueprint(api)


@app.route('/')
@login_required
def index():
    return redirect(url_for('files.view'))


db.create_all()


if app.config['DEBUG']:
    from .models import Users
    if Users.query.filter_by(
        username='admin'
    ).first() is None:
        u=Users(username='admin')
        u.set_password('password')
        db.session.add(u)
        db.session.commit()


if __name__ == '__main__':
    app.run(
        debug=True,
        host=host,
        port=port
    )
