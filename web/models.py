from datetime import datetime, timedelta, timezone
from os import remove as rm, rename as mv, path
from random import choice
from string import hexdigits

from flask_login import UserMixin
from sqlalchemy.exc import IntegrityError
from werkzeug import secure_filename
from werkzeug.security import generate_password_hash, check_password_hash

from .app import db, app


def getnow():
    return datetime.now(tz=timezone(timedelta(hours=-5)))


class Downloads(db.Model):
    id=db.Column(db.Integer, primary_key=True)
    file_id=db.Column(db.Integer, db.ForeignKey('files.id'))
    timestamp=db.Column(db.DateTime(), unique=False, nullable=False)
    ip=db.Column(db.String(128), unique=False)
    success=db.Column(db.Boolean)

    f=db.relationship('Files', backref=db.backref('downloads', lazy=True))


class Otp(db.Model):
    id=db.Column(db.Integer, primary_key=True)
    file_id=db.Column(db.Integer, db.ForeignKey('files.id'))
    otp=db.Column(db.String(128), unique=True, nullable=True)
    creation_timestamp=db.Column(db.DateTime(), nullable=False)
    otp_start=db.Column(db.DateTime(), nullable=True)

    owner=db.relationship('Files', backref=db.backref('otps', lazy=True))

    def gen_otp(self):
        self.otp=''.join(choice(hexdigits) for _ in range(0x10))
        return self.otp

    def check_otp(self, otp):
        tz=timezone(offset=timedelta(hours=-5))
        valid=self.otp == otp and self.creation_timestamp + timedelta(hours=1) >= datetime.now(tz=tz)
        if not valid:
            return False

        if self.otp_start is None:
            self.otp_start=datetime.now(tz=tz)
            return valid

        if not (self.otp_start + timedelta(minutes=app.config['OTP_TIMEOUT']) >= datetime.now(tz=tz)):
            self.otp=None
            self.otp_start=None
            return not valid

        return valid


class Files(db.Model):
    id=db.Column(db.Integer, primary_key=True)
    user_id=db.Column(db.Integer, db.ForeignKey('users.id'))
    filename=db.Column(db.String(128), unique=True)
    public=db.Column(db.Boolean())
    creation_time=db.Column(db.DateTime(), default=getnow, nullable=False)

    owner=db.relationship('Users', backref=db.backref('files', lazy=True))

    def delete_file(self):
        try:
            rm(app.config['UPLOAD_PATH'] + self.filename)
        except FileNotFoundError:
            pass

    def rename(self, new_filename):
        new_filename=secure_filename(new_filename)
        if new_filename == self.filename:
            return True
        if Files.query.filter_by(filename=new_filename).first() is not None:
            return False
        mv(
            app.config['UPLOAD_PATH'] + self.filename,
            app.config['UPLOAD_PATH'] + new_filename,
        )
        self.filename=new_filename
        return True

    def get_link(self):
        return '{}://{}/files/{}'.format(
            app.config['DOMAIN_SCHEMA'],
            app.config['DOMAIN_NAME'],
            self.filename
        )

    def gen_otp(self):
        tz=timezone(offset=timedelta(hours=-5))
        try:
            otp=Otp(
                file_id=self.id,
                creation_timestamp=datetime.now(tz=tz),
            )
            otp.gen_otp()
            db.session.add(otp)
            db.session.commit()
            return otp.otp
        except IntegrityError:
            db.session.rollback()
        return 'error'

    def check_otp(self, user_otp):
        for otp in self.otps:
            if user_otp == otp.otp:
                if otp.check_otp(user_otp):
                    return True
                else:
                    db.session.delete(otp)
                    db.session.commit()
                    break
        return False

    def state(self):
        return {
            'fileId': self.id,
            'filename': self.filename,
            'isPublic': self.public,
            'creationTime': self.creation_time,
            'downloads': len(self.downloads),
            'size': self.size
        }

    @property
    def size(self):
        return self.humansize(path.getsize(app.config['UPLOAD_PATH'] + self.filename))

    @staticmethod
    def humansize(nbytes):
        suffixes=['B', 'KB', 'MB', 'GB', 'TB', 'PB']
        i=0
        f='%d' % nbytes
        while nbytes >= 1024 and i < len(suffixes) - 1:
            nbytes/=1024.
            i+=1
            f=('%.2f' % nbytes).rstrip('0').rstrip('.')
        return '%s %s' % (f, suffixes[i])

    @staticmethod
    def get(filename):
        """this will just get the file object associated with filename"""
        return Files.query.filter_by(filename=filename).first()


class Users(db.Model, UserMixin):
    id=db.Column(db.Integer, primary_key=True)
    username=db.Column(db.String(128), unique=True, index=True)
    password=db.Column(db.String(128), nullable=False)

    def get_id(self):
        return self.username

    def set_password(self, password):
        self.password=generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def group_list(self):
        return ','.join(group.name for group in self.groups)

    def is_admin(self):
        return self.id == 0 and self.username == 'admin'

    @staticmethod
    def get_by(id):
        return Users.query.filter(id == id).first()

    @staticmethod
    def all():
        return Users.query.all()
