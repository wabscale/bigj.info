import os

from flask import Blueprint, request, jsonify
from flask_login import login_required, current_user
from sqlalchemy.exc import IntegrityError
from werkzeug import secure_filename
from functools import wraps

from ..app import app, db
from ..models import Files, Users

api=Blueprint('api', __name__, url_prefix='/api')

def update_files(func):
    @wraps(func)
    def update(*args, **kwargs):
        db_files=list(map(
            lambda x: x.filename,
            Files.query.all()
        ))
        ls_files=os.listdir(app.config['UPLOAD_PATH'])
        new_files=list(filter(
            lambda x: x not in db_files,
            ls_files
        ))
        deleted_files=list(filter(
            lambda x: x not in ls_files,
            db_files
        ))
        admin_user_id=Users.query.filter_by(username='admin').first().id
        for filename in new_files:
            f=Files(
                filename=filename,
                public=False,
                user_id=admin_user_id,
            )
            db.session.add(f)
        for filename in deleted_files:
            f=Files.query.filter_by(filename=filename).first()
            db.session.delete(f)
        db.session.commit()
        return func(*args, **kwargs)

    return update

@api.route('/files/new', methods=['POST'])
@login_required
@update_files
def new():
    error=jsonify({
        'success': False
    }), 500
    file=request.files['file']
    if file is None: return error
    f=Files(
        filename=secure_filename(file.filename),
        public=False,
        user_id=current_user.id,
    )
    file.save(
        os.path.join(
            app.config['UPLOAD_PATH'],
            f.filename
        ),
    )
    db.session.add(f)
    db.session.commit()
    return jsonify({
        'success': True,
        'data': f.state()
    })

@api.route('/files/<fileid>', methods=['GET', 'POST'])
@api.route('/files', methods=['GET', 'POST'])
@login_required
@update_files
def files(fileid=None):
    if fileid is None:
        f=Files.query.all()
        return jsonify({
            'success': True,
            'data'   : list(map(lambda x: x.state(), f))
        })
    f=Files.query.filter_by(
        id=fileid,
    ).first()
    error=jsonify({
        'success': False
    }), 500

    if f is None:
        return error

    if request.method == 'POST':
        json=request.get_json()

        action=json['action'] if 'action' in json else None

        if action == 'update':
            try:
                os.rename(
                    os.path.join(
                        app.config['UPLOAD_PATH'],
                        f.filename
                    ), os.path.join(
                        app.config['UPLOAD_PATH'],
                        json['filename']
                    )
                )
            except FileNotFoundError or OSError:
                return error
            for fname, mname in [
                ('filename', 'filename'),
                ('isPublic', 'public'),
            ]:
                setattr(f, mname, json[fname])
        elif action == 'delete':
            try:
                os.remove(
                    os.path.join(
                        app.config['UPLOAD_PATH'],
                        json['filename']
                    )
                )
            except FileNotFoundError or OSError:
                return error
            db.session.delete(f)
            db.session.commit()
            return jsonify({
                'success': True
            })
        elif action == 'getDownloads':
            print('...')
            return jsonify({
                'success': True,
                'data'   : [
                    '{ip:<12} : {timestamp} {success}'.format(
                        ip=dwnld.ip,
                        timestamp=dwnld.timestamp.strftime('%F %T'),
                        success='' if dwnld.success else 'Denied'
                    ) for dwnld in f.downloads
                ]
            })
        elif action == 'getOtp':
            otp=f.gen_otp()
            if otp != 'error':
                return jsonify({
                    'success': True,
                    'data'   : '{schema}://{domain}/f/{filename}?otp={otp}'.format(
                        schema=app.config['DOMAIN_SCHEMA'],
                        domain=app.config['DOMAIN_NAME'],
                        filename=f.filename,
                        otp=otp,
                    )
                })
            return error

        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return error

    return jsonify({
        'success': True,
        'data'   : f.state()
    })
