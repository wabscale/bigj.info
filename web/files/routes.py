import os
from datetime import datetime, timezone, timedelta
from functools import wraps

from flask import send_from_directory, Blueprint, render_template, request
from flask_login import current_user, login_required
from sqlalchemy.exc import IntegrityError

from ..app import app, db
from ..models import Files, Users, Downloads

files=Blueprint('files', __name__, url_prefix='/f')


def update_files(func):
    @wraps(func)
    def update(*args, **kwargs):
        db_files=list(map(
            lambda x: x.filename,
            Files.query.all()
        ))
        ls_files=os.listdir(app.config['UPLOAD_PATH'])
        new_files=list(filter(
            lambda x: x not in db_files,
            ls_files
        ))
        deleted_files=list(filter(
            lambda x: x not in ls_files,
            db_files
        ))
        admin_user_id=Users.query.filter_by(username='admin').first().id
        for filename in new_files:
            f=Files(
                filename=filename,
                public=False,
                user_id=admin_user_id,
            )
            db.session.add(f)
        for filename in deleted_files:
            f=Files.query.filter_by(filename=filename).first()
            db.session.delete(f)
        db.session.commit()
        return func(*args, **kwargs)

    return update


@files.route('/', methods=['GET', 'POST'])
@login_required
@update_files
def view():
    files=list(map(
        lambda f: f.state(),
        sorted(
            Files.query.filter_by(user_id=current_user.id).all(),
            key=lambda f: f.creation_time,
            reverse=True
        )
    ))
    return render_template(
        'files/view.html',
        files=files,
    )


@files.route('/<path>', methods=['GET'])
@update_files
def serve_file(path):
    tz=timezone(timedelta(hours=-5))
    try:
        file=Files.query.filter_by(filename=path).first()
        if not file.public and not current_user.is_authenticated:
            user_otp=request.args.get('otp', default=None)
            if not file.check_otp(user_otp):
                db.session.commit()
                raise Exception()
        download=Downloads(
            file_id=file.id,
            timestamp=datetime.now(tz=tz),
            ip=request.headers.get('X-Forwarded-For', default=request.remote_addr),
            success=True
        )
        db.session.add(download)
        db.session.commit()
        return send_from_directory(app.config['UPLOAD_PATH'], path)
    except Exception as e:
        print(e)
        db.session.rollback()

    try:
        download=Downloads(
            file_id=file.id,
            timestamp=datetime.now(tz=tz),
            ip=request.headers.get('X-Forwarded-For', default=request.remote_addr),
            success=False
        )
        db.session.add(download)
        db.session.commit()
    except IntegrityError as e:
        print(e)
        db.session.rollback()

    return 'not found', 404
