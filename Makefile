# Python
MAIN_NAME=dev.py
ENV_NAME=venv
PYTHON_VERSION=`which python3`

# Docker
DOCKER_IMAGE_NAME=bigj.icu
DOCKER_OPTIONS=--rm -it -p 5000:5000
# DOCKER_COMPOSE_OPTIONS=--scale private_registry=3

.PHONY: run setup deploy debug dbuild dkill rung clean

all: run

deploy:
	if ! docker network ls | grep traefik-proxy; then \
		docker network create traefik-proxy; \
	fi
	docker-compose build --no-cache
	make dkill
	docker-compose rm -f
	docker-compose up -d --force-recreate ${DOCKER_COMPOSE_OPTIONS}
	# docker kill `docker ps -q --filter "expose=5000" --filter "publish=80"` || true
	# docker-compose run -d -p "127.0.0.1:5000:80" private_registry

debug: kill
	docker-compose up --build

build:
	rm -rf ./web/static/js/*.*.js
	NODE_ENV=production npm run build

dev_build:
	rm -f ./web/static/js/*.*.js
	NODE_ENV=development npm run build

dbuild:
	docker-compose build

dkill:
	docker-compose kill
	docker-compose rm -f

setup_node:
	npm install

setup: setup_node
	if [ -d ${ENV_NAME} ]; then \
		rm -rf ${ENV_NAME}; \
	fi
	if [ -a requirements.txt ]; then \
		touch requirements.txt; \
	fi
	which virtualenv && pip install virtualenv || true
	virtualenv -p ${PYTHON_VERSION} ${ENV_NAME}
	./${ENV_NAME}/bin/pip install -r requirements.txt

rung:
	if [ ! -d ${ENV_NAME} ]; then \
		make setup; \
	fi
	./${ENV_NAME}/bin/gunicorn -b 0.0.0.0:5000 -w 8 web:app #${MAIN_NAME}

run:
	if [ ! -d ${ENV_NAME} ]; then \
		make setup; \
	fi
	./${ENV_NAME}/bin/python ${MAIN_NAME}

jsclean:
	if [ -n "`find ./web/static/js/*.*.js`" ]; then \
		rm -rf `find ./web/static/js/*.*.js`; \
	fi

superclean: clean jsclean
	if [ -d .data ]; then \
		rm -rf .data; \
	fi
	if [ -d /var/lib/f.bigj.info ]; then \
		rm -rf /var/lib/f.bigj.info; \
	fi

clean: jsclean
	docker system prune -f
	if [ -n "`docker image list -q | grep ${DOCKER_IMAGE_NAME}`" ]; then \
		docker rmi ${DOCKER_IMAGE_NAME}; \
	fi
	if [ -d node_modules ]; then \
		rm -rf node_modules; \
	fi
	if [ -d ${ENV_NAME} ]; then \
		rm -rf ${ENV_NAME}; \
	fi
	if [ -n "`find . -name __pycache__`" ]; then \
		rm -rf `find . -name __pycache__`; \
	fi
